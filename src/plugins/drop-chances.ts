(() => {
    const id = 'drop-chances';
    const title = 'Show % Drop Chances';
    const desc =
        'This script injects into the drop chances menu and displays the % chance of receiving any item in the table. Also shows average GP values';
    const imgSrc = getItemMedia(CONSTANTS.item.Crate_Of_Basic_Supplies);

    const dropChances = () => {
        window.viewMonsterDrops = (monsterID) => {
            if (monsterID === -1) monsterID = player.manager.enemy.data.id;
            if (monsterID >= 0 && MONSTERS[monsterID].lootTable !== undefined) {
                let drops = '';
                let dropsOrdered = [];
                let totalWeight = MONSTERS[monsterID].lootTable.reduce((sum, a) => sum + a[1], 0);
                if (MONSTERS[monsterID].lootChance != undefined) {
                    totalWeight = (totalWeight * 100) / MONSTERS[monsterID].lootChance;
                }
                for (let i = 0; i < MONSTERS[monsterID].lootTable.length; i++) {
                    dropsOrdered.push({
                        itemID: MONSTERS[monsterID].lootTable[i][0],
                        w: MONSTERS[monsterID].lootTable[i][1],
                        qty: MONSTERS[monsterID].lootTable[i][2],
                        chance: ` (${((MONSTERS[monsterID].lootTable[i][1] / totalWeight) * 100).toPrecision(3)}%)`,
                    });
                }
                dropsOrdered.sort(function (a, b) {
                    return b.w - a.w;
                });
                for (let i = 0; i < dropsOrdered.length; i++) {
                    drops +=
                        'Up to ' +
                        dropsOrdered[i].qty +
                        ' x <img class="skill-icon-xs mr-2" src="' +
                        getItemMedia(dropsOrdered[i].itemID) +
                        '">' +
                        items[dropsOrdered[i].itemID].name +
                        dropsOrdered[i].chance +
                        '<br>';
                }
                let bones = '';
                if (MONSTERS[monsterID].bones !== null)
                    bones =
                        'Always Drops:<br><small><img class="skill-icon-xs mr-2" src="' +
                        getItemMedia(MONSTERS[monsterID].bones) +
                        '">' +
                        items[MONSTERS[monsterID].bones].name +
                        '</small><br><br>';
                Swal.fire({
                    title: MONSTERS[monsterID].name,
                    html:
                        bones + 'Possible Extra Drops:<br><small>In order of most common to least common<br>' + drops,
                    imageUrl: MONSTERS[monsterID].media,
                    imageWidth: 64,
                    imageHeight: 64,
                    imageAlt: MONSTERS[monsterID].name,
                });
            }
        };

        window.viewItemContents = (itemID = -1) => {
            if (itemID < 0) itemID = selectedBankItem;
            let drops = '';
            let dropsOrdered = [];
            let totalWeight = items[itemID].dropTable.reduce((sum, a) => sum + a[1], 0);
            for (let i = 0; i < items[itemID].dropTable.length; i++) {
                dropsOrdered.push({
                    itemID: items[itemID].dropTable[i][0],
                    w: items[itemID].dropTable[i][1],
                    id: i,
                    chance: ` (${((items[itemID].dropTable[i][1] / totalWeight) * 100).toPrecision(3)}%)`,
                });
            }
            dropsOrdered.sort(function (a, b) {
                return b.w - a.w;
            });
            for (let i = 0; i < dropsOrdered.length; i++) {
                drops +=
                    'Up to ' +
                    numberWithCommas(items[itemID].dropQty[dropsOrdered[i].id]) +
                    ' x <img class="skill-icon-xs mr-2" src="' +
                    getItemMedia(dropsOrdered[i].itemID) +
                    '">' +
                    items[dropsOrdered[i].itemID].name +
                    dropsOrdered[i].chance +
                    '<br>';
            }
            Swal.fire({
                title: items[itemID].name,
                html: 'Possible Items:<br><small>' + drops,
                imageUrl: getItemMedia(itemID),
                imageWidth: 64,
                imageHeight: 64,
                imageAlt: items[itemID].name,
            });
        };

        thievingMenu.showNPCDrops = (npc, area) => {
            const sortedTable = [...npc.lootTable].sort(
                ([itemID, weight, maxQty], [itemID2, weight2, maxQty2]) => weight2 - weight
            );
            const { minGP, maxGP } = game.thieving.getNPCGPRange(npc);
            let html = `<small><img class="skill-icon-xs mr-2" src="${cdnMedia(
                'assets/media/main/coins.svg'
            )}"> ${formatNumber(minGP)}-${formatNumber(maxGP)} GP</small><br>`;
            html += 'Possible Common Drops:<br><small>';
            if (sortedTable.length) {
                html += `In order of most to least common<br>`;
                const totalWeight = game.getLootTableWeight(npc.lootTable);
                html += sortedTable
                    .map(([itemID, weight, maxQty]) => {
                        let text = `${
                            maxQty > 1 ? '1-' : ''
                        }${maxQty} x <img class="skill-icon-xs mr-2" src="${getItemMedia(itemID)}">${
                            items[itemID].name
                        }`;
                        text += ` (${((100 * weight) / totalWeight).toFixed(2)}%)`;
                        return text;
                    })
                    .join('<br>');
                html += `<br>Average Value: ${game.getLootTableAverageGP(npc.lootTable).toFixed(2)} GP<br>`;
            } else {
                html += 'This NPC doesn&apos;t have any Common Drops';
            }
            html += `</small><br>`;
            html += `Possible Rare Drops:<br><small>`;
            html += Thieving.generalRareItems
                .map(({ itemID }) => {
                    return thievingMenu.formatSpecialDrop(items[itemID], 'rare');
                })
                .join('<br>');
            html += `</small><br>`;
            if (area.uniqueDrops.length) {
                html += `Possible Area Unique Drops:<br><small>`;
                html += area.uniqueDrops
                    .map((drop) => thievingMenu.formatSpecialDrop(items[drop.itemID], 'area', drop.qty))
                    .join('<br>');
                html += '</small><br>';
            }
            if (npc.uniqueDrop.itemID !== -1) {
                html += `Possible NPC Unique Drop:<br><small>${thievingMenu.formatSpecialDrop(
                    items[npc.uniqueDrop.itemID],
                    'npc',
                    npc.uniqueDrop.qty
                )}</small>`;
            }
            Swal.fire({
                title: npc.name,
                html,
                imageUrl: npc.media,
                imageWidth: 64,
                imageHeight: 64,
                imageAlt: npc.name,
            });
        };

        thievingMenu.formatSpecialDrop = (item, type, qty = 1) => {
            const found = itemStats[item.id].stats[Stats.TimesFound];
            const media = found ? getItemMedia(item.id) : cdnMedia('assets/media/main/question.svg');
            const name = found ? item.name : 'Undiscovered Item';

            // Calculates the odds of a special drop
            let specialDropChance = 0;
            switch (type) {
                case 'rare':
                    specialDropChance = Thieving.generalRareItems.filter((x) => x.itemID == item.id)[0]?.chance;
                    break;
                case 'area':
                    const chanceMult = game.thieving.isPoolTierActive(3) ? 3 : 1;
                    specialDropChance = game.thieving.areaUniqueChance * chanceMult;
                    break;
                case 'npc':
                    specialDropChance = game.thieving.pickpocket;
                    break;
                default:
                    console.log('Drop Chances Error');
            }

            return `${formatNumber(
                qty
            )} x <img class="skill-icon-xs mr-2" src="${media}">${name} (${specialDropChance.toFixed(3)}%)`;
        };
    };

    let loadCheckInterval = setInterval(() => {
        if (isLoaded) {
            clearInterval(loadCheckInterval);
            dropChances();
        }
    }, 200);

    SEMI.add(id, {
        ms: 0,
        pluginType: SEMI.PLUGIN_TYPE.TWEAK,
        title,
        desc,
        imgSrc,
        onEnable: dropChances,
    });
})();
