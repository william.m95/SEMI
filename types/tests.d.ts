declare function testWeaponTypes(): void;
declare function testAttacks(): void;
declare function compareCombatSerialization(): void;
declare let serialCombatState: number[];
declare function freezeAndSerialize(): void;
declare function deserializeAndUnFreeze(): void;
declare function timeFunctionCall(func: VoidFunction, numCalls?: number): void;
declare function deleteAllSaves(): void;
