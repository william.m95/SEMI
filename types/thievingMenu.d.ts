/** Menu class for thieving */
declare class ThievingMenu {
    private container;
    private areaPanels;
    private npcNavs;
    private activePanel;
    constructor(containerID: string);
    private createInfoBox;
    updateNPCsForLevel(level: number): void;
    updateNPCButtons(): void;
    selectNPC(npc: ThievingNPC, area: ThievingArea): void;
    private selectNPCInPanel;
    updateAllAreaPanels(): void;
    setStopButton(area: ThievingArea): void;
    removeStopButton(): void;
    private updateAreaPanelInfo;
    private updateInfoContainerForNPC;
    private showNPCDrops;
    private formatSpecialDrop;
    getProgressBar(area: ThievingArea): ProgressBar;
}
declare type ThievingAreaPanel = {
    panelContainer: HTMLDivElement;
    targetContainer: HTMLDivElement;
    infoContainer: HTMLDivElement;
    infoBoxName: HTMLElement;
    infoBoxImage: HTMLImageElement;
    startButton: HTMLButtonElement;
    dropsButton: HTMLButtonElement;
    progressBar: ProgressBar;
    infoBox: ThievingInfoBox;
    selectedNPC: ThievingNPC | -1;
    area: ThievingArea;
};
declare type ThievingInfoBox = {
    container: HTMLDivElement;
    xp: XPIcon;
    interval: IntervalIcon;
    stealth: StealthIcon;
    double: DoublingIcon;
    masteryXP: MasteryXPIcon;
    poolXP: MasteryPoolIcon;
};
declare type ThievingNPCNav = {
    button: HTMLAnchorElement;
    panel: ThievingAreaPanel;
    buttonContent: HTMLDivElement;
    success: HTMLElement;
    maxHit: HTMLElement;
    unlock: HTMLDivElement;
};
